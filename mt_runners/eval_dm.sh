#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J eval_dm
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=20GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=24:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A dlp1
## Specyfikacja partycji
#SBATCH -p plgrid-gpu
#SBATCH --gres=gpu:1
## Plik ze standardowym wyjściem
#SBATCH --output="/net/people/plgpwawryka/masters_thesis/scratch/outputs/eval_dm_sig.out"

## przejscie do glownego katalogu programu
cd /net/people/plgpwawryka/masters_thesis/model

## zaladowanie wymaganych bibliotek
module add apps/cuda/8.0
module add plgrid/tools/python/2.7.9

STORAGE=/net/archive/groups/plggdlp/pwawryka
SCRATCH=/net/scratch/people/plgpwawryka

INPUT_DATA_PATTERN=${STORAGE}/frame_level/small_validate/validate*.tfrecord
OUTPUT_DIR=${SCRATCH}/validate/vec_test_1k
MODEL_DIR=${SCRATCH}/model/vec_test_1k

EPOCH_NUM=10
LEARNING_RATE=0.005
GLOBAL_BATCH_SIZE=5120
BATCH_SIZE=256
VECTOR_SIZE=1024

## wykonanie skryptu
python dm_eval.py --input_data_pattern=${INPUT_DATA_PATTERN} --output_dir=${OUTPUT_DIR} --model_dir=${MODEL_DIR} --epoch_num=${EPOCH_NUM} --vector_size=${VECTOR_SIZE}
