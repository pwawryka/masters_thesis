#!/usr/bin/env bash

sbatch eval_video_dbof_avg_hub.sh
sbatch eval_video_dbof_avg_sig.sh

sbatch eval_video_dbof_con_hub.sh
sbatch eval_video_dbof_con_sig.sh

sbatch eval_video_dbof_sin_hub.sh
sbatch eval_video_dbof_sin_sig.sh
