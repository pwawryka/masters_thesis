#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J eval_video_std
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=20GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=2:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A dlp1
## Specyfikacja partycji
#SBATCH -p plgrid-gpu
#SBATCH --gres=gpu:1
## Plik ze standardowym wyjściem
#SBATCH --output="/net/people/plgpwawryka/masters_thesis/scratch/outputs/eval_video_std.out"

BASE_DIR=/net/people/plgpwawryka/masters_thesis

## przejscie do glownego katalogu programu
cd $BASE_DIR/model

## zaladowanie wymaganych bibliotek
module add apps/cuda/8.0
module add plgrid/tools/python/2.7.9

## wykonanie skryptu
python eval.py --eval_data_pattern=$BASE_DIR'/storage/video_level/validate/validate*.tfrecord' --train_dir=$BASE_DIR/scratch/video_std --run_once=True

