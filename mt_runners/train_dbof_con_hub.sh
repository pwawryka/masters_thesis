#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J train_dbof_con_hub
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=35GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=12:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A dlp1
## Specyfikacja partycji
#SBATCH -p plgrid-gpu
#SBATCH --gres=gpu:1
## Plik ze standardowym wyjściem
#SBATCH --output="/net/people/plgpwawryka/masters_thesis/scratch/outputs/train_dbof_con_hub.out"

## przejscie do glownego katalogu programu
cd /net/people/plgpwawryka/masters_thesis/model

## zaladowanie wymaganych bibliotek
module add apps/cuda/8.0
module add plgrid/tools/python/2.7.9

STORAGE=/net/archive/groups/plggdlp/pwawryka
SCRATCH=/net/scratch/people/plgpwawryka

INPUT_DATA_PATTERN=${STORAGE}/frame_level/train/train*.tfrecord
OUTPUT_DIR=${SCRATCH}/train/dbof_con_hub
MODEL_DIR=${SCRATCH}/model/dbof_con_hub

EPOCH_NUM=10
LEARNING_RATE=0.05
GLOBAL_BATCH_SIZE=5120
BATCH_SIZE=256
VECTOR_SIZE=1024
DATASET_SIZE=750000
MODEL_TYPE=concat
LOSS=huber

## wykonanie skryptu
python dbof_train.py --input_data_pattern=${INPUT_DATA_PATTERN} --output_dir=${OUTPUT_DIR} --model_dir=${MODEL_DIR} --epoch_num=${EPOCH_NUM} --vector_size=${VECTOR_SIZE} --global_batch_size=${GLOBAL_BATCH_SIZE} --dataset_size=${DATASET_SIZE} --model_type=${MODEL_TYPE} --loss=${LOSS}
