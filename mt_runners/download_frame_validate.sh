#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J download_frame_validate
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=1GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=24:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A dlp1
## Specyfikacja partycji
#SBATCH -p plgrid
## Plik ze standardowym wyjściem
#SBATCH --output="scratch/outputs/download_frame_validate.out"
 
 
## przejscie do katalogu z ktorego wywolany zostal sbatch
cd /net/archive/groups/plggdlp/pwawryka/frame_level/validate

curl data.yt8m.org/download.py | partition=1/frame_level/validate mirror=us python 

