#!/bin/bash -l
## Nazwa zlecenia
#SBATCH -J train_video_dm_moe_avg_hub
## Liczba alokowanych węzłów
#SBATCH -N 1
## Liczba zadań per węzeł (domyślnie jest to liczba alokowanych rdzeni na węźle)
#SBATCH --ntasks-per-node=1
## Ilość pamięci przypadającej na jeden rdzeń obliczeniowy (domyślnie 5GB na rdzeń)
#SBATCH --mem-per-cpu=32GB
## Maksymalny czas trwania zlecenia (format HH:MM:SS)
#SBATCH --time=6:00:00
## Nazwa grantu do rozliczenia zużycia zasobów
#SBATCH -A dlp1
## Specyfikacja partycji
#SBATCH -p plgrid-gpu
#SBATCH --gres=gpu:1
## Plik ze standardowym wyjściem
#SBATCH --output="/net/people/plgpwawryka/masters_thesis/scratch/outputs/train_video_dm_moe_avg_hub.out"

BASE_DIR=/net/people/plgpwawryka/masters_thesis

## przejscie do glownego katalogu programu
cd ${BASE_DIR}/model

## zaladowanie wymaganych bibliotek
module add apps/cuda/8.0
module add plgrid/tools/python/2.7.9

STORAGE=/net/archive/groups/plggdlp/pwawryka
SCRATCH=/net/scratch/people/plgpwawryka

## wykonanie skryptu
python video_dm_train.py --train_data_pattern=${SCRATCH}'/train/avg_hub/*.tfrecord' --train_dir=${BASE_DIR}/scratch/model/video_dm_avg_hub --model=MoeModel --start_new_model=True
