#!/usr/bin/env python
import os
import re
import sys

import numpy as np
import matplotlib.pyplot as plt


def parse_file(path):
    with open(path, 'r') as f:
        content = f.read()
    pattern = r'Loss: ([0-9]+\.[0-9]+)'
    result = re.findall(pattern, string=content)
    return np.asarray(map(float, result), dtype=np.float32)


def plot_result(name, data):
    plt.title(name)
    plt.plot(np.arange(data.size) + 1, data)
    plt.show()


def main():
    results_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'results')
    files = os.listdir(results_dir)

    args = set(sys.argv[1:])
    if args:
        files = [f for f in files for arg in args if arg in f]

    for filename in files:
        path = os.path.join(results_dir, filename)
        name = filename.split('.')[0]
        data = parse_file(path)
        plot_result(name, data)


if __name__ == '__main__':
    main()
