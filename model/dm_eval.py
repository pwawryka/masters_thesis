import os
import traceback

import tensorflow as tf

from tensorflow import logging, flags

from master.data import DataReader, DataProviderFactory, DataWriter, DataContext
from master.model import DistributedMemoryModel
from master.sample_vector_watcher import SampleVectorWatcher


# Define program arguments
FLAGS = flags.FLAGS
flags.DEFINE_string('input_data_pattern', None, 'Input data pattern')
flags.DEFINE_string('output_dir', None, 'Directory for calculated features')
flags.DEFINE_string('model_dir', None, 'Directory for model')
flags.DEFINE_integer('batch_size', 256, 'Batch size')
flags.DEFINE_integer('global_batch_size', 1024, 'Global batch size')
flags.DEFINE_integer('vector_size', 300, 'Video vector size')
flags.DEFINE_float('learning_rate', 0.005, 'Learning rate')
flags.DEFINE_integer('epoch_num', 10, 'Number of training epochs')
flags.DEFINE_string('loss', 'x-entropy', 'Loss function')
flags.DEFINE_integer('dataset_size', int(1e6), 'Dataset chunk size')
flags.DEFINE_string('model_type', 'con', 'Model type [con | avg]')

# Setup logging
logging.set_verbosity(logging.INFO)
logging.info('DM started...')
logging.info('input_data_pattern: %s', FLAGS.input_data_pattern)
logging.info('output_dir: %s', FLAGS.output_dir)
logging.info('model_dir: %s', FLAGS.model_dir)
logging.info('batch_size: %s', FLAGS.batch_size)
logging.info('global_batch_size: %s', FLAGS.global_batch_size)
logging.info('vector_size: %s', FLAGS.vector_size)
logging.info('learning_rate: %s', FLAGS.learning_rate)
logging.info('epoch_num: %s', FLAGS.epoch_num)
logging.info('loss: %s', FLAGS.loss)
logging.info('dataset_size: %s', FLAGS.dataset_size)
logging.info('model_type: %s', FLAGS.model_type)

# Create input nodes
input_tensors = DataReader().get_input_data_tensors(
    FLAGS.input_data_pattern,
    FLAGS.global_batch_size,
    FLAGS.epoch_num,
    num_readers=1
)

# Data context
data_context = DataContext()

# Create data provider factory
provider_factory = DataProviderFactory(FLAGS.batch_size)

# Create model instance
model = DistributedMemoryModel(
    FLAGS.dataset_size,
    FLAGS.batch_size,
    FLAGS.learning_rate,
    FLAGS.loss,
    FLAGS.vector_size
)
model.setup_eval()              # This is evaluation
model.load(FLAGS.model_dir)     # Load model variables as constants
model.build(FLAGS.model_type)

# Summary
merged_summary = tf.summary.merge_all()
model.summary_op = merged_summary

# Sample watcher
# vector_watcher = SampleVectorWatcher(FLAGS.output_dir, dataset_size=FLAGS.dataset_size, samples_num=5)
if not os.path.exists(FLAGS.output_dir):
    os.makedirs(FLAGS.output_dir)

with tf.Session() as sess:
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    summary_writer = tf.summary.FileWriter(FLAGS.model_dir, graph=sess.graph)

    try:
        global_batch_step = 0

        while not coord.should_stop():
            global_batch_step += 1

            video_ids_val, video_matrix_val, labels_val, num_frames_val = sess.run(input_tensors)
            ids_val = data_context.process(video_ids_val, labels_val)
            provider = provider_factory.create(ids_val, video_matrix_val, labels_val, num_frames_val)

            loss, summary = 0, None
            for indices, image, audio in provider:
                loss, summary = model.loop(indices, image, sess)
                # vector_watcher.watch(indices, model.video_matrix, sess)

            logging.info('Global step %s | Loss: %s', global_batch_step, loss)

            if summary is not None:
                summary_writer.add_summary(summary, global_batch_step)

    except tf.errors.OutOfRangeError:
        logging.info('Data loader finished')
        coord.request_stop()
    except Exception as e:
        logging.info('Unexpected error occurs')
        logging.info('Error message: %s', e.message)
        logging.info(traceback.format_exc())
        coord.request_stop()
    finally:
        coord.join(threads)
        summary_writer.close()
        DataWriter(FLAGS.output_dir).write(sess, data_context, model.video_matrix)
        logging.info('Exiting...')
