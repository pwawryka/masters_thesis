import tensorflow as tf


# Model type mixins

class DBoFAverageMixin(object):
    def _get_weights_size(self):
        return [self.VIDEO_VECTOR_SIZE, self.IMAGE_FRAME_SIZE]

    def _get_biases_size(self):
        return [self.IMAGE_FRAME_SIZE]

    def _build_output(self):
        self.output = (self.frame_1 + self.frame_2 + self.frame_3 + self.frame_4) / 4


class DBoFConcatMixin(object):
    def _get_weights_size(self):
        return [self.VIDEO_VECTOR_SIZE, 2 * self.IMAGE_FRAME_SIZE]

    def _get_biases_size(self):
        return [2 * self.IMAGE_FRAME_SIZE]

    def _build_output(self):
        self.output = tf.concat([self.frame_1, self.frame_2], axis=1)


class DBoFSingleMixin(object):
    def _get_weights_size(self):
        return [self.VIDEO_VECTOR_SIZE, self.IMAGE_FRAME_SIZE]

    def _get_biases_size(self):
        return [self.IMAGE_FRAME_SIZE]

    def _build_output(self):
        self.output = tf.identity(self.frame_1)


# Loss function mixins

class DBoFHuberMixin(object):
    @staticmethod
    def huber_loss(y_true, y_pred, max_grad=1.):
        """Calculates the huber loss.

        Parameters
        ----------
        y_true: np.array, tf.Tensor
          Target value.
        y_pred: np.array, tf.Tensor
          Predicted value.
        max_grad: float, optional
          Positive floating point value. Represents the maximum possible
          gradient magnitude.

        Returns
        -------
        tf.Tensor
          The huber loss.
        """
        err = tf.abs(y_true - y_pred, name='abs')
        mg = tf.constant(max_grad, name='max_grad')
        lin = mg * (err - .5 * mg)
        quad = .5 * err * err
        return tf.where(err < mg, quad, lin)

    def _build_loss(self):
        loss_function = self.huber_loss(self.output, self.result),
        self.loss = tf.reduce_mean(tf.reduce_sum(loss_function, axis=1))


class DBoFSigmoidMixin(object):
    def _build_loss(self):
        loss_function = tf.nn.sigmoid_cross_entropy_with_logits(labels=self.output, logits=self.result)
        self.loss = tf.reduce_mean(tf.reduce_sum(loss_function, axis=1))
