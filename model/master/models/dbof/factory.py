from master.models.dbof.variants import (
    DBoFSingleHuber, DBoFSingleSigmoid,
    DBoFAverageHuber, DBoFAverageSigmoid,
    DBoFConcatHuber, DBoFConcatSigmoid
)


class DBoFModelFactory(object):
    FACTORY_MAP = {
        'single': {
            'huber': DBoFSingleHuber,
            'sigmoid': DBoFSingleSigmoid
        },
        'concat': {
            'huber': DBoFConcatHuber,
            'sigmoid': DBoFConcatSigmoid
        },
        'average': {
            'huber': DBoFAverageHuber,
            'sigmoid': DBoFAverageSigmoid
        }
    }

    def __init__(self, model_type, loss_function):
        self.model_type = model_type
        self.loss_function = loss_function

        assert model_type in self.FACTORY_MAP.keys()
        assert loss_function in self.FACTORY_MAP[model_type].keys()

    def initialize(self, *args, **kwargs):
        return self.FACTORY_MAP[self.model_type][self.loss_function](*args, **kwargs)
