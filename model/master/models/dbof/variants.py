from master.models.dbof.base import DBoFModelBase
from master.models.dbof.mixins import (
    DBoFSingleMixin, DBoFConcatMixin, DBoFAverageMixin,
    DBoFHuberMixin, DBoFSigmoidMixin
)


class DBoFSingleHuber(DBoFSingleMixin, DBoFHuberMixin, DBoFModelBase):
    pass


class DBoFSingleSigmoid(DBoFSingleMixin, DBoFSigmoidMixin, DBoFModelBase):
    pass


class DBoFConcatHuber(DBoFConcatMixin, DBoFHuberMixin, DBoFModelBase):
    pass


class DBoFConcatSigmoid(DBoFConcatMixin, DBoFSigmoidMixin, DBoFModelBase):
    pass


class DBoFAverageHuber(DBoFAverageMixin, DBoFHuberMixin, DBoFModelBase):
    pass


class DBoFAverageSigmoid(DBoFAverageMixin, DBoFSigmoidMixin, DBoFModelBase):
    pass
