import tensorflow as tf

from master.serializer import ModelSerializer


class DBoFModelBase(ModelSerializer):
    IMAGE_FRAME_SIZE = 1024

    frame_1, frame_2, frame_3, frame_4 = [None] * 4
    matrix = None
    indexes = None
    lookup = None
    weights = None
    biases = None
    result = None
    output = None
    loss = None
    train = None

    serialize_fields = ['weights', 'biases']

    def __init__(self, dataset_size, batch_size, learning_rate, loss, vector_size=None, make_summary=False):
        self._dataset_size = dataset_size       # Whole dataset size
        self._batch_size = batch_size           # Single batch size
        self._learning_rate = learning_rate
        self._loss_function = loss
        self._make_summary = make_summary       # True if you want tensorboard summary
        self.__train = True                     # Training / Evaluation
        self.__initialize_from_file = False

        self.VIDEO_VECTOR_SIZE = vector_size or self.VIDEO_VECTOR_SIZE

    def setup_train(self):
        self.__train = True

    def setup_eval(self):
        self.__train = False

    def setup_initialize_from_file(self):
        self.__initialize_from_file = True

    def _build_output(self):
        raise NotImplementedError

    def _get_weights_size(self):
        raise NotImplementedError

    def _get_biases_size(self):
        raise NotImplementedError

    def _build_loss(self):
        raise NotImplementedError

    def _build_weights(self):
        if self.__train:
            if self.__initialize_from_file:
                _weights = self.model_data['weights']
                self.weights = tf.Variable(_weights)
            else:
                self.weights = tf.Variable(tf.random_normal(self._get_weights_size(), stddev=0.01))
        else:
            _weights = self.model_data['weights']
            self.weights = tf.constant(_weights)

    def _build_biases(self):
        if self.__train:
            if self.__initialize_from_file:
                _biases = self.model_data['biases']
                self.biases = tf.Variable(_biases)
            else:
                self.biases = tf.Variable(tf.zeros(self._get_biases_size()))
        else:
            _biases = self.model_data['biases']
            self.biases = tf.constant(_biases)

    def build(self):
        self.matrix = tf.Variable(tf.zeros([self._dataset_size, self.VIDEO_VECTOR_SIZE]), name='matrix')
        self.indexes = tf.placeholder(tf.int32, [self._batch_size], name='indexes')
        self.lookup = tf.nn.embedding_lookup(self.matrix, self.indexes, name='lookup')

        self._build_weights()
        self._build_biases()
        self.result = tf.add(tf.matmul(self.lookup, self.weights), self.biases)

        self.frame_1 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE])
        self.frame_2 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE])
        self.frame_3 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE])
        self.frame_4 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE])

        self._build_output()
        self._build_loss()

        self.train = tf.train \
            .GradientDescentOptimizer(self._learning_rate) \
            .minimize(self.loss, name='train')

    def loop(self, indexes, image, sess):
        feed_dict = {
            self.indexes: indexes,
            self.frame_1: image[:, 0, :],
            self.frame_2: image[:, 1, :],
            self.frame_3: image[:, 2, :],
            self.frame_4: image[:, 3, :]
        }
        _, loss = sess.run([self.train, self.loss], feed_dict=feed_dict)
        return loss
