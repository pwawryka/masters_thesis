import tensorflow as tf

from master.serializer import ModelSerializer


class DistributedMemoryModel(ModelSerializer):
    VIDEO_VECTOR_SIZE = 300
    IMAGE_FRAME_SIZE = 1024
    AUDIO_FRAME_SIZE = 128

    video_matrix = None
    video_matrix_reset = None
    video_indexes = None
    video_lookup = None

    image_frame_1 = None
    image_frame_2 = None
    image_frame_3 = None
    image_frame_4 = None
    image_weights = None
    image_input_concat = None
    image_frame_result = None
    image_loss = None
    image_train = None

    audio_frame_1 = None
    audio_frame_2 = None
    audio_frame_3 = None
    audio_frame_4 = None
    audio_weights = None
    audio_input_concat = None
    audio_frame_result = None
    audio_loss = None
    audio_train = None

    summary_op = None

    serialize_fields = ['image_weights', 'image_biases']
    # serialize_fields = ['image_weights', 'audio_weights']

    def __init__(self, dataset_size, batch_size, learning_rate, loss, vector_size=None, make_summary=False):
        self._dataset_size = dataset_size       # Whole dataset size
        self._batch_size = batch_size           # Single batch size
        self._learning_rate = learning_rate
        self._loss_function = loss
        self._make_summary = make_summary       # True if you want tensorboard summary
        self.__train = True                     # Training / Evaluation
        self.__initialize_from_file = False

        self.VIDEO_VECTOR_SIZE = vector_size or self.VIDEO_VECTOR_SIZE

    def setup_train(self):
        self.__train = True

    def setup_eval(self):
        self.__train = False

    def setup_initialize_from_file(self):
        self.__initialize_from_file = True

    def _build_base(self):
        # video matrix
        self.video_matrix = tf.Variable(
            tf.zeros([self._dataset_size, self.VIDEO_VECTOR_SIZE]),
            name='video_matrix'
        )
        self.video_indexes = tf.placeholder(tf.int32, [self._batch_size], name='video_indexes')
        self.video_lookup = tf.nn.embedding_lookup(self.video_matrix, self.video_indexes, name='video_lookup')

        if self._make_summary:
            tf.summary.histogram('video_matrix__histogram', self.video_matrix)

    def _build_image_weights(self, size):
        if self.__train:
            if self.__initialize_from_file:
                _image_weights = self.model_data['image_weights']
                self.image_weights = tf.Variable(_image_weights, name='image_weights')
            else:
                self.image_weights = tf.Variable(tf.random_normal(size, stddev=0.01), name='image_weights')
        else:
            _image_weights = self.model_data['image_weights']
            self.image_weights = tf.constant(_image_weights, name='image_weights')

    def _build_image_biases(self):
        if self.__train:
            if self.__initialize_from_file:
                _image_biases = self.model_data['image_biases']
                self.image_biases = tf.Variable(_image_biases, name='image_biases')
            else:
                self.image_biases = tf.Variable(tf.zeros([self.IMAGE_FRAME_SIZE]), name='image_biases')
        else:
            _image_biases = self.model_data['image_biases']
            self.image_biases = tf.constant(_image_biases, name='image_biases')

    @staticmethod
    def huber_loss(y_true, y_pred, max_grad=1.):
        """Calculates the huber loss.

        Parameters
        ----------
        y_true: np.array, tf.Tensor
          Target value.
        y_pred: np.array, tf.Tensor
          Predicted value.
        max_grad: float, optional
          Positive floating point value. Represents the maximum possible
          gradient magnitude.

        Returns
        -------
        tf.Tensor
          The huber loss.
        """
        err = tf.abs(y_true - y_pred, name='abs')
        mg = tf.constant(max_grad, name='max_grad')
        lin = mg * (err - .5 * mg)
        quad = .5 * err * err
        return tf.where(err < mg, quad, lin)

    def _build_image_loss(self):
        if self._loss_function == 'huber':
            loss_function = self.huber_loss(self.image_frame_4, self.image_frame_result)
        elif self._loss_function == 'x-entropy':
            loss_function = tf.nn .sigmoid_cross_entropy_with_logits(
                labels=self.image_frame_4, logits=self.image_frame_result, name='cross_entropy'
            )
        else:
            raise ValueError('Loss function should be "huber" or "x-entropy"')
        self.image_loss = tf.reduce_mean(tf.reduce_sum(loss_function, axis=1), name='image_loss')

    def _build_con_model(self):
        self.avg_frame = (self.image_frame_1 + self.image_frame_2 + self.image_frame_3) / 3

        self.image_input_concat = tf.concat(
                [self.avg_frame, self.video_lookup],
                axis=1,
                name='image_input_concat'
        )

        self._build_image_biases()

        _image_weights_size = [1 * self.IMAGE_FRAME_SIZE + self.VIDEO_VECTOR_SIZE, self.IMAGE_FRAME_SIZE]
        self._build_image_weights(_image_weights_size)

        self.image_frame_result = tf.add(
            tf.matmul(self.image_input_concat, self.image_weights),
            self.image_biases,
            name='image_frame_result'
        )

    def _build_avg_model(self):
        self.avg_frame = (self.image_frame_1 + self.image_frame_2 + self.image_frame_3 + self.video_lookup) / 4

        self._build_image_biases()

        _image_weights_size = [1 * self.IMAGE_FRAME_SIZE, self.IMAGE_FRAME_SIZE]
        self._build_image_weights(_image_weights_size)

        self.image_frame_result = tf.add(
            tf.matmul(self.avg_frame, self.image_weights),
            self.image_biases,
            name='image_frame_result'
        )

    def _build_image(self, model_type):
        # input image frames
        self.image_frame_1 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE], name='image_frame_1')
        self.image_frame_2 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE], name='image_frame_2')
        self.image_frame_3 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE], name='image_frame_3')

        # output image frame
        self.image_frame_4 = tf.placeholder(tf.float32, [self._batch_size, self.IMAGE_FRAME_SIZE], name='image_frame_4')

        if model_type == 'con':
            self._build_con_model()
        elif model_type == 'avg':
            self._build_avg_model()
        else:
            raise ValueError('Unknown model type argument')

        self._build_image_loss()

        self.image_train = tf.train \
            .GradientDescentOptimizer(self._learning_rate) \
            .minimize(self.image_loss, name='image_train_op')

        if self._make_summary:
            tf.summary.histogram('image_weights', self.image_weights)
            tf.summary.histogram('image_loss', self.image_loss)
            tf.summary.histogram('image_weights', self.image_weights)
            tf.summary.histogram('image_biases', self.image_biases)
            tf.summary.scalar('image_loss', self.image_loss)

    def _build_audio(self):
        # input audio frames
        self.audio_frame_1 = tf.placeholder(tf.float32, [self._batch_size, self.AUDIO_FRAME_SIZE])
        self.audio_frame_2 = tf.placeholder(tf.float32, [self._batch_size, self.AUDIO_FRAME_SIZE])
        self.audio_frame_3 = tf.placeholder(tf.float32, [self._batch_size, self.AUDIO_FRAME_SIZE])

        # output audio frame
        self.audio_frame_4 = tf.placeholder(tf.float32, [self._batch_size, self.AUDIO_FRAME_SIZE])

        # audio weights (batch_size x (3 * frame_size + video_vector_size))
        _audio_weights_size = [3 * self.AUDIO_FRAME_SIZE + self.VIDEO_VECTOR_SIZE, self.AUDIO_FRAME_SIZE]
        self.audio_weights = tf.Variable(tf.zeros(_audio_weights_size))

        self.audio_input_concat = tf.concat(
                [self.audio_frame_1, self.audio_frame_2, self.audio_frame_3, self.video_lookup], 1
        )
        self.audio_frame_result = tf.matmul(self.audio_input_concat, self.audio_weights)

        self.audio_loss = tf.reduce_mean(tf.losses.mean_squared_error(self.audio_frame_4, self.audio_frame_result))

        self.audio_train = tf.train.AdamOptimizer(self._learning_rate).minimize(self.audio_loss)

    def build(self, model_type):
        self._build_base()
        self._build_image(model_type)
        # self._build_audio()

    def loop(self, indexes, image, sess):
        feed_dict = {
            self.video_indexes: indexes,
            self.image_frame_1: image[:, 0, :],
            self.image_frame_2: image[:, 1, :],
            self.image_frame_3: image[:, 2, :],
            self.image_frame_4: image[:, 3, :],
        }

        if self._make_summary and self.summary_op is not None:
            _, image_loss, summary = sess.run(
                [self.image_train, self.image_loss, self.summary_op],
                feed_dict=feed_dict
            )
        else:
            _, image_loss = sess.run(
                [self.image_train, self.image_loss],
                feed_dict=feed_dict
            )
            summary = None

        return image_loss, summary
