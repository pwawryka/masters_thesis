import os
import random

import tensorflow as tf


class SampleVectorWatcher(object):
    DEFAULT_SAMPLES_NUM = 5

    def __init__(self, directory, dataset_size, samples_num=None):
        self._directory = os.path.join(directory, 'samples')
        self._samples = set(random.sample(range(dataset_size), samples_num or self.DEFAULT_SAMPLES_NUM))
        self._prepare_directory()

        tf.logging.info('Watching samples: %s', ','.join(map(str, self._samples)))

    def _sample_file(self, sample_id):
        return os.path.join(self._directory, '%s.txt' % sample_id)

    def _prepare_directory(self):
        if not os.path.exists(self._directory):
            os.makedirs(self._directory)
        for sample_id in self._samples:
            with open(self._sample_file(sample_id), 'w+'):
                pass

    def _append_line(self, sample_id, vector):
        with open(self._sample_file(sample_id), 'a') as _file:
            _file.write('{vec}\n'.format(vec=','.join(map(str, vector))))

    def watch(self, indices, video_matrix, sess):
        indices_set = set(indices)
        if not indices_set.intersection(self._samples):
            return

        video_matrix_val = sess.run(video_matrix)
        for sample_id in self._samples:
            if sample_id in indices_set:
                self._append_line(sample_id, video_matrix_val[sample_id])
