import os
import pickle

import tensorflow as tf


class ModelSerializer(object):
    serialize_fields = []
    model_data = None

    def load(self, model_dir):
        data = {}
        for field in self.serialize_fields:
            path = os.path.join(model_dir, '%s.data' % field)
            with open(path, 'r') as _file:
                data[field] = pickle.load(_file)
                tf.logging.info('Loaded %s data', field)

        self.model_data = data
        return data

    def save(self, sess, model_dir):
        for field in self.serialize_fields:
            path = os.path.join(model_dir, '%s.data' % field)
            with open(path, 'w+') as _file:
                data = getattr(self, field).eval(sess)
                pickle.dump(data, _file)
                tf.logging.info('Saved %s data', field)
