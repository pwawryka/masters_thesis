import os
import time
import functools

import tensorflow as tf


def save_job_params(flags):
    params = flags.__dict__['__flags']
    directory = params['model_dir']
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = os.path.join(directory, 'job_params.txt')
    with open(filename, 'w+') as f:
        for key, value in params.iteritems():
            f.write('{key} = {value}\n'.format(key=key, value=value))


class Timer(object):
    info_template = 'Timer %s, %s'

    def __init__(self, name=None):
        self._name = name
        self._start = None

    def __enter__(self):
        self._start = time.clock()

    def __exit__(self, exc_type, exc_val, exc_tb):
        end = time.clock()
        tf.logging.info(self.info_template, self._name, end - self._start)

    def __call__(self, func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start = time.clock()
            result = func(*args, **kwargs)
            end = time.clock()
            tf.logging.info(self.info_template, self._name, end - start)
            return result

        return wrapper
