import tensorflow as tf


class DataReader(object):
    num_classes = 4716
    feature_names = ['rgb', 'audio']
    feature_sizes = [1024, 128]
    max_frames = 300

    max_quantized_value = 1.0
    min_quantized_value = 0.0

    def get_input_data_tensors(self, data_pattern, global_batch_size, num_epochs=1, num_readers=1):
        with tf.name_scope('input_data'):
            files = tf.gfile.Glob(data_pattern)

            # Divide dataset in large chunks to fit video vectors inside GPU memory
            chunk_slice = slice(3600, 5400)
            tf.logging.info('Data file slice: %s - %s', chunk_slice.start, chunk_slice.stop)
            files = sorted(files)[chunk_slice]

            if not files:
                raise IOError('Unable to find training files. data_pattern={}'.format(data_pattern))
            filename_queue = tf.train.string_input_producer(files, num_epochs=num_epochs, shuffle=True)
            training_data = [self._prepare_reader(filename_queue) for _ in range(num_readers)]
            return tf.train.batch_join(
                training_data,
                batch_size=global_batch_size,
                capacity=global_batch_size * 2,
                allow_smaller_final_batch=True,
                enqueue_many=True
            )

    def _prepare_reader(self, filename_queue):
        reader = tf.TFRecordReader()
        _, serialized_example = reader.read(filename_queue)

        return self._prepare_serialized_examples(serialized_example)

    def _prepare_serialized_examples(self, serialized_example):
        contexts, features = tf.parse_single_sequence_example(
            serialized_example,
            context_features={
                'video_id': tf.FixedLenFeature([], tf.string),
                'labels': tf.VarLenFeature(tf.int64)
            },
            sequence_features={
                feature_name: tf.FixedLenSequenceFeature([], dtype=tf.string) for feature_name in self.feature_names
            }
        )

        # read ground truth labels
        labels = tf.cast(
                tf.sparse_to_dense(contexts['labels'].values, (self.num_classes,), 1, validate_indices=False),
                tf.bool
        )

        num_features = len(self.feature_names)
        num_frames = -1  # the number of frames in the video
        feature_matrices = [None] * num_features  # an array of different features
        for feature_index in range(num_features):
            feature_matrix, num_frames_in_this_feature = self._get_video_matrix(
                    features[self.feature_names[feature_index]],
                    self.feature_sizes[feature_index],
                    self.max_frames
            )
            if num_frames == -1:
                num_frames = num_frames_in_this_feature
            else:
                tf.assert_equal(num_frames, num_frames_in_this_feature)

            feature_matrices[feature_index] = feature_matrix

        # cap the number of frames at self.max_frames
        num_frames = tf.minimum(num_frames, self.max_frames)

        # concatenate different features
        video_matrix = tf.concat(feature_matrices, 1)

        # convert to batch format.
        batch_video_ids = tf.expand_dims(contexts['video_id'], 0)
        batch_video_matrix = tf.expand_dims(video_matrix, 0)
        batch_labels = tf.expand_dims(labels, 0)
        batch_frames = tf.expand_dims(num_frames, 0)

        return batch_video_ids, batch_video_matrix, batch_labels, batch_frames

    def _get_video_matrix(self, features, feature_size, max_frames):
        decoded_features = tf.reshape(tf.cast(tf.decode_raw(features, tf.uint8), tf.float32), [-1, feature_size])

        num_frames = tf.minimum(tf.shape(decoded_features)[0], max_frames)
        feature_matrix = self._dequantize(decoded_features)
        feature_matrix = self._resize_axis(feature_matrix, 0, max_frames)
        return feature_matrix, num_frames

    def _dequantize(self, feat_vector):
        quantized_range = self.max_quantized_value - self.min_quantized_value
        scalar = quantized_range / 255.0
        # bias = (quantized_range / 512.0) + self.min_quantized_value
        # return feat_vector * scalar + bias
        return feat_vector * scalar

    @staticmethod
    def _resize_axis(tensor, axis, new_size, fill_value=0):
        tensor = tf.convert_to_tensor(tensor)
        shape = tf.unstack(tf.shape(tensor))

        pad_shape = shape[:]
        pad_shape[axis] = tf.maximum(0, new_size - shape[axis])

        shape[axis] = tf.minimum(shape[axis], new_size)
        shape = tf.stack(shape)

        resized = tf.concat([
          tf.slice(tensor, tf.zeros_like(shape), shape),
          tf.fill(tf.stack(pad_shape), tf.cast(fill_value, tensor.dtype))
        ], axis)

        # Update shape.
        new_shape = tensor.get_shape().as_list()  # A copy is being made.
        new_shape[axis] = new_size
        resized.set_shape(new_shape)
        return resized
