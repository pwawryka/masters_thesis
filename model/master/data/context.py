class DataContext(object):
    def __init__(self):
        self._id = 0
        self._map = {}
        self._video_ids = []
        self._labels = []

    def process(self, video_ids, labels):
        result = []
        for video_id, video_labels in zip(video_ids, labels):
            if video_id not in self._map:
                self._map[video_id] = self._id
                self._video_ids.append(video_id)
                self._labels.append(video_labels)
                self._id += 1
            result.append(self._map[video_id])
        return result

    @property
    def last_id(self):
        return self._id

    @property
    def video_ids(self):
        return self._video_ids

    @property
    def labels(self):
        return self._labels
