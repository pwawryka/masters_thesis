import numpy as np
import tensorflow as tf


class DataProviderFactory(object):
    def __init__(self, batch_size):
        self.batch_size = batch_size

    def create(self, *args, **kwargs):
        return DataProvider(self.batch_size, *args, **kwargs)


class DataProvider(object):
    # number of frames to model input
    FRAMES_TO_INPUT = 4

    # how many times one video has been process through network
    ITERATIONS_PER_VIDEO = 10

    def __init__(self, batch_size, video_id, model_input, labels, num_frames):
        self.batch_size = batch_size

        self.video_id = video_id
        self.model_input = model_input
        self.labels = labels
        self.num_frames = num_frames

        self._current_iteration = 0
        self._current_index = 0

    def _partial(self, index):
        # Get video batch as matrix (batch_size x num_frames x feature_len)
        end_index = index + self.batch_size
        matrix = self.model_input[index:end_index]
        num_frames = self.num_frames[index:end_index] - self.FRAMES_TO_INPUT

        num_frames = np.clip(num_frames, self.FRAMES_TO_INPUT, 300)

        # Setup proper indexes (different num_frames for each video)
        feature_indexes = np.random.uniform(low=0, high=num_frames, size=self.batch_size).astype(np.int32)
        # feature_indexes.fill(self._current_iteration * self.FRAMES_TO_INPUT)
        # feature_indexes %= num_frames

        # Prepare input matrix (batch_size x FRAME_TO_INPUT x feature_len)
        input_matrix = np.array([
            video_matrix[video_index:video_index + self.FRAMES_TO_INPUT, :]
            for video_index, video_matrix
            in zip(feature_indexes, matrix)
        ])

        # TODO: Occasionally something is wrong with input matrix shape
        #       Need to investigate
        #       Probably 'num_frames = np.clip(num_frames, self.FRAMES_TO_INPUT, 300)'
        #       fixes the problem
        try:
            image, audio = input_matrix[:, :, :1024], input_matrix[:, :, 1024:]
        except IndexError:
            tf.logging.info('Index error, input matrix shape %s', input_matrix.shape)
            tf.logging.info('Whole model input shape %s', self.model_input.shape)
            tf.logging.info('features_indexes %s', feature_indexes)
            tf.logging.info('num_frames %s', num_frames)
            image, audio = None, None

        return self.video_id[index:end_index], image, audio

    def __iter__(self):
        return self

    def next(self):
        if self._current_index + self.batch_size <= self.model_input.shape[0]:
            matrix_index, self._current_index = self._current_index, self._current_index + self.batch_size
            return self._partial(matrix_index)

        if self._current_iteration < self.ITERATIONS_PER_VIDEO:
            self._current_iteration += 1
            self._current_index = 0
            return self.next()

        raise StopIteration()
