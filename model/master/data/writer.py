import os

import numpy as np
import tensorflow as tf

from master.utils import Timer


class DataWriter(object):
    MAX_RECORDS_PER_FILE = 2048

    def __init__(self, output_dir=None, chunk_id=0):
        self._chunk_id = 2
        self._output_dir = output_dir or ''
        self._file_index = 0

    def write(self, sess, context, video_matrix):
        video_matrix_val = sess.run(video_matrix)
        video_ids = context.video_ids
        labels = context.labels
        for index in xrange(0, video_matrix_val.shape[0], self.MAX_RECORDS_PER_FILE):
            self._write_file(
                video_ids[index:index+self.MAX_RECORDS_PER_FILE],
                labels[index:index+self.MAX_RECORDS_PER_FILE],
                video_matrix_val[index:index+self.MAX_RECORDS_PER_FILE]
            )

    @Timer('file_writer')
    def _write_file(self, video_ids, labels, video_features):
        if not np.any(video_features):
            tf.logging.info('Empty video features. File will not be created.')
            return

        filename = os.path.join(self._output_dir, self._next_filename())
        tf.logging.info('Writing file %s with movie vectors', filename)
        writer = tf.python_io.TFRecordWriter(filename)
        for vid_id, vid_labels, vid_features in zip(video_ids, labels, video_features):
            if np.any(vid_features):
                record = tf.train.Example(features=tf.train.Features(feature={
                    'video_id': tf.train.Feature(bytes_list=tf.train.BytesList(value=[vid_id])),
                    'labels': tf.train.Feature(int64_list=tf.train.Int64List(value=list(np.where(vid_labels)[0]))),
                    'video_vector': tf.train.Feature(float_list=tf.train.FloatList(value=list(vid_features.astype(np.float))))
                }))
                writer.write(record.SerializeToString())
        writer.close()
        tf.logging.info('Writer finished file %s', filename)

    def _next_filename(self):
        self._file_index += 1
        return 'chunk_{}_file_{}.tfrecord'.format(self._chunk_id, self._file_index)
